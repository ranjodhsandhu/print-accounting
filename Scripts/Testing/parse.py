import csv
import os
import logging
logFileName = './Logs/test.log'
logging.basicConfig(filename = logFileName, filemode='a', format ='%(asctime)s %(message)s')
logg = logging.getLogger()
logg.setLevel(logging.DEBUG)
userIDDatabase = 'list.ini'
papercutdata = 'print_summary_by_user.csv'
def firstParse(papercutdata):
        reader = csv.reader(open(papercutdata), lineterminator='\n')
        writer = csv.writer(open('firstParse.csv', 'w'))
        next(reader)
        next(reader)
        next(reader)
        writer.writerow(['UserID', 'Cost'])
        for row in reader:
                writer.writerow([row[0], format(float(row[12]), '.2f')])
        logg.info('First Parse completed into shortened CSV')
def secondParse(userIDDatabase):                        
        writer = csv.writer(open('parse.csv', 'w')) 
        writer.writerow(['9DigitID', 'Amount'])
        reader = csv.reader(open('firstParse.csv'), lineterminator='\n') 
        UserIDs = []
        costs = []
        next(reader)
        for row in reader:
                UserIDs.append('['+row[0]+']')
                costs.append(row[1])
        notFound = set(UserIDs)
        lines = (line.rstrip() for line in open(userIDDatabase))
        for line in lines:
                for i in range(len(UserIDs)):
                        if UserIDs[i] == line:
                                writer.writerow([next(lines)[4:], costs[i], UserIDs[i][1:-1]])
                                notFound.remove(UserIDs[i])             
        with open('notFound.txt', 'w') as file1:
                for i in range(len(notFound)): 
                        file1.write(list(notFound)[i][1:-1]+'\n')
                        logg.error(list(notFound)[i][1:-1] + ' not found in database. Written to notFound.txt')
        logg.info('Second Parse completed with computing ID, user ID, balance')
firstParse(papercutdata)
secondParse(userIDDatabase)
os.remove('firstParse.csv')
