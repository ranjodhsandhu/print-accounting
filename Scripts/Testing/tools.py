import csv
import os
import datetime
#given data from papercut and user database ini creates master csv and notes missing IDs
def parseDatabase(papercutdata, userIDDatabase):
    reader0 = csv.reader(open(papercutdata), lineterminator='\n')
    writer0 = csv.writer(open('firstParse.csv', 'w'))
    next(reader0)
    next(reader0)
    next(reader0)
    writer0.writerow(['UserID', 'Cost'])
    for row in reader0:
        writer0.writerow([row[0], format(float(row[12]), '.2f')])
    writer = csv.writer(open('parse.csv', 'w')) 
    writer.writerow(['9DigitID', 'Amount'])
    reader = csv.reader(open('firstParse.csv'), lineterminator='\n') 
    UserIDs = []
    costs = []
    next(reader)
    for row in reader:
        UserIDs.append('['+row[0]+']')
        costs.append(row[1])
    notFound = set(UserIDs)
    lines = (line.rstrip() for line in open(userIDDatabase))
    for line in lines:
        for i in range(len(UserIDs)):
            if UserIDs[i] == line:
                writer.writerow([next(lines)[4:], costs[i], UserIDs[i][1:-1]])
                notFound.remove(UserIDs[i])             
    with open('notFound.txt', 'w') as file1:
        for i in range(len(notFound)): 
            file1.write(list(notFound)[i][1:-1]+'\n')
    os.remove('firstParse.csv')

def getMonth():
    printSummary = 'print_summary_by_user.csv'
    pSummaryFile = open(printSummary)
    readPSummary = csv.reader(pSummaryFile, lineterminator='\n')
    next(readPSummary)
    for row in readPSummary:
        lst = row[0].split()
        month = lst[4]
        year = lst[6][2:]
        break
    fullmonth = datetime.datetime.strptime(month,'%b').strftime('%B')
    intmonth = datetime.datetime.strptime(month,'%b').strftime('%m')
    return fullmonth

def getYear():
    printSummary = 'print_summary_by_user.csv'
    pSummaryFile = open(printSummary)
    readPSummary = csv.reader(pSummaryFile, lineterminator='\n')
    next(readPSummary)
    for row in readPSummary:
        lst = row[0].split()
        month = lst[4]
        year = lst[6][2:]
        break
    return year
