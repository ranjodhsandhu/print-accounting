<?php

  CLASS ERROR {
    var $Description;
    var $Line;
    var $File;
    
    function ERROR($description, $line=0, $file='') {
      $this->Description = $description;
      $this->Line = $line;
      $this->File = $file;
    }
  }

?>
