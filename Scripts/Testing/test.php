<?php
    require_once('SARCMail.inc');
    require_once('print_billing.msg');
$File = 'parse.csv';

$arrResult  = array();
$handle     = fopen($File, "r");

if(empty($handle) === false) {
    while(($data = fgetcsv($handle)) !== FALSE){
        $arrResult[] = $data;
    }
    fclose($handle);
}
$fp = file('parse.csv');
$c = count($fp);
for ($x = 1; $x < $c; $x++ ){
    $compID = $arrResult[$x][2];
    $balance = $arrResult[$x][1];
    SARC_MAIL(array(  'Subject' => '[ACCOUNT INFO] SARC Printing System',
                                    'To'      => "$compID@virginia.edu",
                                    //'Cc'      => 'arch-request@virginia.edu',
                                    //'Bcc'     => 'Ranjodh Sandhu <rss6py@virginia.edu>',
                                    'From'    => 'SARC Help Desk<arch-request@virginia.edu>',
                                    'HTML'    => sprintf(SARC_EMAIL_BILLING_HTML, "$compID", $balance, date('j M Y', strtotime('+1 day') ) ),
                                    'text'    => sprintf(SARC_EMAIL_BILLING_TEXT, "$compID", $balance, date('j M Y', strtotime('+1 day') ) ) ) );
    echo("Email sent to $compID \n");
}
?>