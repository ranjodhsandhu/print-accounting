<?php

/***************************************************************************************************
**                                                                                                **
**  Logging Facility                                                                              **
**                                                                                                **
**  NEW LOG( (int)type, (string)location )                                                        **
**    type - 0: No logging                                                                        **
**           1: Log to terminal                                                                   **
**           2: Log to file                                                                       **
**                                                                                                **
***************************************************************************************************/

CLASS LOG {

  const NONE      = 0x0;
  const TERMINAL  = 0x1;
  const FILE      = 0x2;
  
  const MODULE = 'Logging.inc';
  
  private $type;
  private $location;
  
  public function __call($func, $args) {
    
    if($func != 'entry') throw new Exception('Invalid function');
    
    switch((int)$this->type) {
      
      case LOG::TERMINAL :
        $this->log_TERMINAL($args[0]);
        break;
      
      case LOG::FILE :
        $this->log_FILE($args[0]);
        break;
      
      case LOG::NONE :
        return;
        break;
      
    }
    
  }

  function LOG($type, $location = '') {
  
    switch((int)$type) {
    
      //  Log to terminal
      case LOG::TERMINAL: 
      
        $this->type = LOG::TERMINAL;
        break;
        
        
      //  Log to file
      case LOG::FILE: 
      
        //  Try opening file
        //    if failure log to terminal
              if(!($this->location = fopen($location, 'ab'))) {
                $this->type = LOG::TERMINAL;
                throw new Exception(MODULE."Error opening file '$location'. Logging to terminal instead.");
              }
              
        //    if success
              else {
                $this->type = LOG::FILE;
              }
              
        break;
        
        
      //  No logging
      case LOG::NONE: 
      default:
      
        $this->type = LOG::NONE;
        break;
      
    }
  
  }
  
  
  function close() {
    
    if($this->location) fclose( $this->location );
    
  }
  
  function log_TERMINAL($entry = '') { echo "$entry\n"; }
  
  function log_FILE($entry = '') { fwrite( $this->location, date('Y-m-d H:i:s') . " -- $entry\r\n"); }

}




CLASS LOGGING {
  
  private $log_PROCESS;
  private $log_ERROR;
  private $log_DEBUG;
  
  const MODULE = 'Logging.inc';
  
  private $Logs;
  
  function Process($event) { $this->Logs->PROCESS->entry($event); }
  function Error($event) { $this->Logs->ERROR->entry($event); }
  function Debug($event) { $this->Logs->DEBUG->entry($event); }
  
  
  
  function LOGGING() {
    
    $this->Logs = (object)array('PROCESS' => new LOG(LOG::NONE),
                                'ERROR'   => new LOG(LOG::NONE),
                                'DEBUG'   => new LOG(LOG::NONE) );
    
  }
  
  
  function set_Location($log, $log_type, $log_location = '') {
    
    if((int)$log_type < LOG::NONE || (int)$log_type > LOG::FILE) {
      
      throw new Exception('(' . LOGGING::MODULE . ':' . __LINE__ . ') ' . "Invalid Type. ($log_type)");
      
    }
    else {
      
      $log = strtoupper($log);
      
      if(!isset($this->Logs->{$log})) {
        
        throw new Exception('(' . LOGGING::MODULE . ':' . __LINE__ . ') ' . "Invalid Log. ($log)");
        
      }
      
      try {
        
        $this->Logs->{$log} = new LOG($log_type, $log_location);
        
      }
      catch(Exception $e) {
        
        throw $e;
        
      }
      
    }
    
  }
  
  
  function close() {
    
    $this->Logs->PROCESS->close();
    $this->Logs->ERROR->close();
    $this->Logs->DEBUG->close();
    
  }
  
  
}



?>