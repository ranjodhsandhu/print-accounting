import os
from tools import getMonth, getYear
import logging
logFileName = './Logs/test.log'
logging.basicConfig(filename = logFileName, filemode='a', format ='%(asctime)s %(message)s')
logg = logging.getLogger()
logg.setLevel(logging.DEBUG)
fullmonth = getMonth()
year = getYear()
name = 'SARCPrinting_20'+year+fullmonth+'.prn'
logg.info('Fixed width txt converted to .prn')
os.rename('fw.txt', name)

os.remove('fw.csv')
os.rename("./Logs/test.log", "./Logs/"+fullmonth+"20"+year+"_Print.log")