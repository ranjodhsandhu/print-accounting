<?php

  
  CLASS PARSEFILE {
    
    const default_FS = "\t";
    const default_LS = '/[\n\r]+/';
    const MODULE = 'ParseFile.inc';
    const DEBUG = 0;
    
  
    //  Functions
    //    Parse File
          function parse ($options) {
        
            global $LOGS;
      
            
            //  Set Defaults
            $FS = PARSEFILE::default_FS;
            $LS = PARSEFILE::default_LS;
            $col_line = false;
            
            //  Parse Options
            foreach($options as $option=>$value) {
              switch(strtoupper($option)) {
                case 'SEPARATOR' :
                  $FS = $value;
                  break;
                
                case 'LINE-SEPARATOR' :
                  $LS = $value;
                  break;
                  
                case 'FILE' :
                  if(!($contents = file_get_contents(($file = $value)))) {
                    
                    //  ERROR CODE
                    $LOGS->Error('(' . PARSEFILE::MODULE . ':' . __LINE__ . ') ' . "Could not get contents file ($value).");
                    return false;
                    
                  }
                  break;
                
                case 'COLUMN-LINE' :
                  $col_line = $value == true;
                  break;
              }
            }
            
            //  Parse File
            $rtn = array();
            $lines = preg_split($LS, $contents,NULL,PREG_SPLIT_NO_EMPTY);
            
            if($col_line) {
              $columns = explode($FS, array_shift($lines));
              
              foreach($lines as $line) {
                $rtn[] = array_combine($columns, explode($FS, $line));
              }
            }
            else foreach($lines as $line) $rtn[] = explode($FS, $line);
            
            if(PARSEFILE::DEBUG) $LOGS->Debug('(' . PARSEFILE::MODULE . ':' . __LINE__ . ') ' . "Parsed file ($file).");
            return $rtn;
          }
  }
  
  
  
  
  function parse_delimited ($options) {
        
    global $LOGS;
      
  
    //  Options
        $separator = "\t";
        $columns = array();
    
    //  Parse Options
        foreach($options as $option=>$value) {
          switch(strtoupper($option)) {
            case 'SEPARATOR' :
              $separator = $value;
              break;
            
            case 'FILE' :
              if(is_file($value))
                $contents = file($value, FILE_IGNORE_NEW_LINES);
              break;
              
            case 'COLUMNS' :
              $col_names = $value;
              break;
          }
        }
    
    //  Parse File
        if(!isset($contents) || !count($contents)) {
          
          //  ERROR CODE
          $LOGS->Error('(' . PARSEFILE::MODULE . ':' . __LINE__ . ') ' . "Could not get contents file ($file).");
          return false;
          
        }
        
        $rtn = array();
        $ln = 0;
        
        //  Get Columns
            if(count($col_names)) {
            
        //    Advance to the first line with Alphanumeric characters
              while(strlen(preg_replace("/[^a-z0-9]/i", "", $contents[$ln])) == '') ++$ln;
        
        //    Get column positions
              $values = explode($separator, $contents[$ln]);
              foreach($col_names as $col=>$col_name) {
                if(($pos = array_search($col_name, $values)) !== false) $columns[$col] = $pos;
              }
            
            }

        
        //  Parse lines
            for(++$ln; $ln < count($contents); ++$ln) {
              $values = explode($separator, $contents[$ln]);
              if(count($values) < count($columns)) continue;
              
              if(count($columns)) {
                $row = array();
                foreach($columns as $column_title=>$column_position) {
                  $row[$column_title] = $values[$column_position];
                }
                $rtn[] = $row;
              }
              else {
                $rtn[] = $values;
              }
            }
    
    
        //  Return array
            if(PARSEFILE::DEBUG) $LOGS->Debug('(' . PARSEFILE::MODULE . ':' . __LINE__ . ') ' . "Parsed file ($file).");
            return $rtn;
  }
  
  
  
  function parse_csv ($options) {
    
    global $LOGS;
    
    //  Options
        $FS = ',';
        $FE = '"';
        $CL = FALSE;
        
        foreach ($options as $option=>$value) {
          switch (strtoupper($option)) {
            case 'FILE' :
              if (is_file($value))
                $FILE = fopen($value, 'r');
              break;
            
            case 'SEPARATOR' :
              $FS = $value[0];
              break;
              
            case 'LINE' :
              $LS = $value[0];
              break;
            
            case 'ENCLOSURE' :
              $FE = $value[0];
              break;
                
            case 'COLUMN-LINE' :
              $CL = $value == true;
              break;
          }
        }
        
        $rtn = array();
        
        if ($CL) {
          $columns = fgetcsv($FILE, 250, $FS, $FE);
          $columns[0] = preg_replace('/[^,a-zA-Z:]/','',$columns[0]);
        }
                
        while (($data = fgetcsv($FILE, 250, $FS, $FE)) !== FALSE) {
          if ($CL && count($columns) == count($data)) $rtn[] = array_combine($columns, $data);
          else $rtn[] = $data;
        }
        
        if(PARSEFILE::DEBUG) $LOGS->Debug('(' . PARSEFILE::MODULE . ':' . __LINE__ . ') ' . "Parsed file ($options[FILE]).");
        return $rtn;
    
  }

?>