import csv
import os
import datetime
import logging

parsedCSV = 'parse.csv'
papercut = 'papercutconvert.csv'
fwcsv = 'fw.csv'
printSummary = 'print_summary_by_user.csv'
logFileName = './Logs/test.log'


logging.basicConfig(filename = logFileName, filemode='a', format ='%(asctime)s %(message)s')
logg = logging.getLogger()
logg.setLevel(logging.DEBUG)


pSummaryFile = open(printSummary)
readPSummary = csv.reader(pSummaryFile, lineterminator='\n')
next(readPSummary)
for row in readPSummary:
    lst = row[0].split()
    month = lst[4]
    year = lst[6][2:]
    break


global fullmonth
fullmonth = datetime.datetime.strptime(month,'%b').strftime('%B')
intmonth = datetime.datetime.strptime(month,'%b').strftime('%m')
code = '251311003004'
if intmonth < 6:
    term = "1"+year+"2"
elif intmonth < 8:
    term = "1"+year+"6"
else:
    term = "1"+year+"8"

comment = 'SARC Printing (20' + year + ' ' + fullmonth+')'
termheader = '00037'


numLines = 0
amount = 0.00
pCSV = open(parsedCSV)
pCut = open(papercut, 'w')
pCSVReader = csv.reader(pCSV, lineterminator='\n')
pCutWriter = csv.writer(pCut)
next(pCSVReader)
pCutWriter.writerow(['9digitID','Total','Amount','code','term','comment'])
for row in pCSVReader:
    pCutWriter.writerow([row[0],'',row[1], code, term ,comment])
    numLines += 1
    amount += float(row[1])
amount = round(amount, 2)
amount = format(amount, '.2f')
line = ['', numLines, amount,'', termheader, '']
pCSV.close()
pCut.close()
papercutReader = csv.reader(open(papercut), lineterminator='\n')
fwcsvWriter = csv.writer(open(fwcsv, 'w'))
rows = []
for row in papercutReader:
    rows.append(row)
rows.insert(1, line)
fwcsvWriter.writerows(rows)
logg.info('Fixed width csv template created')
os.remove(papercut)
    