import os
from tools import getMonth, getYear
import logging
import csv 

logFileName = './Logs/test.log'
logging.basicConfig(filename = logFileName, filemode='a', format ='%(asctime)s %(message)s')
logg = logging.getLogger()
logg.setLevel(logging.DEBUG)
fullmonth = getMonth()
name = 'pc_adjust_'+fullmonth+'.bat'
readParse = csv.reader(open("parse.csv"), lineterminator='\n')
writer = open(name, 'w')
next(readParse)
for line in readParse:
    writer.write("server-command adjust-user-account-balance " + line[2] + " " + line[1] + ' "' + fullmonth + ' charges billed to SIS" \n')
logg.info(".bat file created.")