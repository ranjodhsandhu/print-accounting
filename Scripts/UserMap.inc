<?php

  require_once("\\\\Administrative.arch.virginia.edu\Administrative\NetAdmin\scripts\PHP\include\Equitrac.inc");

  CLASS USER_MAP {
  
    const MODULE = 'UserMap.inc';
    const put_User_Map = "[%s]\nUID=%s\nName=\"%s\"\nLastUpdate=\"%s\"\n\n";
    const DEBUG = 0;
    
    public $map;
    
    private $file;
    private $expiration;
    private $values = array( "UID", "Name", "LastUpdate" );
    
    
    //  Constructor
        function USER_MAP($file, $expire = '9 months ago') {
          
          global $LOGS;
          
          $this->expiration = $expire;
          $this->file = $file;
          if(!is_file($this->file)) {
            
            //  ERROR CODE
            $LOGS->Error('(' . USER_MAP::MODULE . ':' . __LINE__ . ') ' . "Cannot find User Map file '$this->file'.");
            return false;
            
          }
          if(!$this->map = parse_ini_file($this->file, true)) {
            
            //  ERROR CODE
            $LOGS->Error('(' . USER_MAP::MODULE . ':' . __LINE__ . ') ' . "Cannot parse User Map file '$this->file'.");
            return false;
            
          }
          else {
            
            if(USER_MAP::DEBUG) $LOGS->Debug('(' . USER_MAP::MODULE . ':' . __LINE__ . ') ' . "Parsed User Map file '$this->file'.");
            
          }
        }
    
    
    //  Functions
    //    Write Map file
          function writeMap() {
          
            global $LOGS;
          
            $test_date = date("Y-m-d", strtotime($this->expiration));
            ksort($this->map);
            if(!($file = fopen($this->file, "w"))) {
              
              //  ERROR CODE
              $LOGS->Error('(' . USER_MAP::MODULE . ':' . __LINE__ . ') ' . "Cannot open User Map file for writing. ($this->file)");
              return false;
              
            }
            
            foreach($this->map as $student=>$map) {
            
              if($map["LastUpdate"] < $test_date) {
              
                unset($this->map[$student]);
                continue;
                
              }
              
              if(!fwrite($file, sprintf(USER_MAP::put_User_Map, $student, $map["UID"], $map["Name"], $map["LastUpdate"]))) {
                
                //  ERROR CODE
                $LOGS->Error('(' . USER_MAP::MODULE . ':' . __LINE__ . ') ' . "Cannot write User Map file. ($this->file)");
                return false;
                
              }
              
            }
            if(USER_MAP::DEBUG) $LOGS->Debug('(' . USER_MAP::MODULE . ':' . __LINE__ . ') ' . "Updated User Map file '$this->file'.");
            
            //  Add new Equitrac Users
                if(!$EQdb = new EQUITRAC()) {
                  
                  // ERROC CODE
                  $LOGS->Error('(' . USER_MAP::MODULE . ':' . __LINE__ . ') ' . "Cannot open Equitrac connection.");
                  
                }
                else {
                  
                  if(!$EQdb->update_users($this->map)) {
                    
                    //  ERROR CODE
                    return false;
                    
                  }
                  
                }
                
            fclose($file);
          }
    
    //    Get User
          function get_user($UID) {
          
            global $LOGS;
          
            if(isset($this->map[$UID])) return $this->map[$UID];
            $LOGS->Error('(' . USER_MAP::MODULE . ':' . __LINE__ . ') ' . "User '$UID' not found.");
          }
    
    //    Set User
          function set_user($info) {
          
            global $LOGS;
            
            if(!isset($info['UID']) || !isset($info['CompID'])) {
              
              //  ERROR CODE
              $LOGS->Error('(' . USER_MAP::MODULE . ':' . __LINE__ . ') ' . "Either or both the UID and Computing ID were not supplied.");
              
            }
            else {
              
              foreach($this->values as $value) if(isset($info[$value])) $this->map[$info['CompID']][$value] = preg_replace("/ *, */", ", ", $info[$value]);
              
            }
          }
    
    
    //  Get All Users
        function get_all_users() {
        
          global $LOGS;
          $students = array();
            
          foreach($this->map as $student=>$map) {
            
            $students[] = $student;
            
          }
          
          return $students;
          
        }
  }
  
?>