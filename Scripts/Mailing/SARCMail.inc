<?php

  //require_once('\\\\Administrative.arch.virginia.edu\Administrative\NetAdmin\scripts\PHP\include\error.inc');


  function SARC_MAIL($options) {
  
    global $LOGS;
    
    
    //  Parse options
        foreach($options as $option=>$value) {
          switch(strtoupper($option)) {
            case 'SUBJECT' :
            case 'FROM' :
            case 'TO' :
            case 'CC' :
            case 'BCC' :
            case 'TEXT' :
            case 'HTML' :
              ${strtolower($option)} = $value;
              break;
          }
        }
    
    
    //  Check for invalid values
        if(!$subject) {
        
          $LOGS->Error('(SARC_MAIL:' . __LINE__ . ') ' . "Email must have a subject");
          return false;
        
        }
        
        if(!preg_match("/[a-z0-9!#$%&'\*\+\-\/=\?\^_`\{\|\}\~][a-z0-9!#$%&'\*\+\-\/=\?\^_`\{\|\}\~\.]*@[a-z0-9!#$%&'\*\+\-\/=\?\^_`\{\|\}\~]+\.[a-z0-9!#$%&'\*\+\-\/=\?\^_`\{\|\}\~\.]+/i", $to)) {
        
          $LOGS->Error('(SARC_MAIL:' . __LINE__ . ') ' . "Invalid TO: '$to'");
          return false;
        
        }
        
        if(!preg_match("/[a-z0-9!#$%&'\*\+\-\/=\?\^_`\{\|\}\~][a-z0-9!#$%&'\*\+\-\/=\?\^_`\{\|\}\~\.]*@[a-z0-9!#$%&'\*\+\-\/=\?\^_`\{\|\}\~]+\.[a-z0-9!#$%&'\*\+\-\/=\?\^_`\{\|\}\~\.]+/i", $from)) {
        
          $LOGS->Error('(SARC_MAIL:' . __LINE__ . ') ' . "Invalid FROM: '$from'");
          return false;
        
        }
        
        if(isset($cc) && !preg_match("/[a-z0-9!#$%&'\*\+\-\/=\?\^_`\{\|\}\~][a-z0-9!#$%&'\*\+\-\/=\?\^_`\{\|\}\~\.]*@[a-z0-9!#$%&'\*\+\-\/=\?\^_`\{\|\}\~]+\.[a-z0-9!#$%&'\*\+\-\/=\?\^_`\{\|\}\~\.]+/i", $cc)) {
        
          $LOGS->Error('(SARC_MAIL:' . __LINE__ . ') ' . "Invalid CC: '$cc'");
          return false;
        
        }
        
        if(isset($bcc) && !preg_match("/[a-z0-9!#$%&'\*\+\-\/=\?\^_`\{\|\}\~][a-z0-9!#$%&'\*\+\-\/=\?\^_`\{\|\}\~\.]*@[a-z0-9!#$%&'\*\+\-\/=\?\^_`\{\|\}\~]+\.[a-z0-9!#$%&'\*\+\-\/=\?\^_`\{\|\}\~\.]+/i", $bcc)) {
        
          $LOGS->Error('(SARC_MAIL:' . __LINE__ . ') ' . "Invalid BCC: '$bcc'");
          return false;
        
        }
        
        if(!isset($text) && !isset($html)) {
        
          $LOGS->Error('(SARC_MAIL:' . __LINE__ . ') ' . "Email must have a body");
          return false;
        
        }
    
    
    //  Send Mail
        $today = date('Y-m-d[H:i:s]');
        if(isset($html)) {
          //$html = preg_replace("/[\n\r]+ */","", $html);
          //$lines = str_split($html, 67);
          //$html_body = implode("=\n", $lines);
          //$html_body = preg_replace("/=([^\n]{1})/", "=3D$1", $html_body);
          $html_body = quoted_printable_encode($html);
          
          $headers = "MIME-Version: 1.0;\r\n";
          $headers.= "Content-Type: multipart/alternative;  boundary=\"SARCMail_$today\";\r\n";
          $headers.= "X-Mailer: PHP/".phpversion().";\r\n";

          if(isset($text)) {
            $message = <<<EOT
--SARCMail_$today
Content-Type: text/plain; charset="utf-8"
Content-Transfer-Encoding: 8bit

$text

--SARCMail_$today
Content-Type: text/html; charset="utf-8"
Content-Transfer-Encoding: quoted-printable

$html_body


--SARCMail_$today--
';
EOT;
          }
          else {
            $message = <<<EOT
--SARCMail_$today
Content-Type: text/html; charset="utf-8"
Content-Transfer-Encoding: quoted-printable

$html_body


--SARCMail_$today--
';
EOT;
          }
        }
        else {
          $message = $text;
          $headers = '';
        }
        
        $headers.= "To: $to\r\n";
        $headers.= "From: $from\r\n";
        if(isset($cc)) $headers.= "CC: $cc\r\n";
        if(isset($bcc)) $headers.= "BCC: $bcc\r\n";
        
        if(!mail(preg_replace('/.*<(.*)>/', '\1', $to), $subject, $message, $headers)) {
        
          $LOGS->Error('(SARC_MAIL:' . __LINE__ . ') ' . "Eroor sending to '$to'");

        }

  }

?>