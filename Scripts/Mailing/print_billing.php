<?php
  
//  Inclusion files    
    require_once('\\\\Administrative.arch.virginia.edu\ADMINISTRATIVE\NetAdmin\scripts\PHP\include\logging.inc');
    require_once('\\\\Administrative.arch.virginia.edu\ADMINISTRATIVE\NetAdmin\scripts\PHP\include\UserMap.inc');
    require_once('\\\\Administrative.arch.virginia.edu\ADMINISTRATIVE\NetAdmin\scripts\PHP\include\Equitrac.inc');
    require_once('\\\\Administrative.arch.virginia.edu\ADMINISTRATIVE\NetAdmin\scripts\PHP\include\SARCMail.inc');
    require_once('\\\\Administrative.arch.virginia.edu\ADMINISTRATIVE\NetAdmin\scripts\PHP\include\ParseFile.inc');
    require_once('\\\\Administrative.arch.virginia.edu\ADMINISTRATIVE\NetAdmin\scripts\PHP\include\print_billing.msg');
  
  
//  Constants
    const EQUITRAC_CHARGES = 'adjust,ur,%s,%.2f,"SIS Billing Adjustment"';
    const EQUITRAC_MINIMUM = 'modify,ur,%s,!,-500';
    const SIS_HEADER = '          %-9u%-10.10s           %-5s';
    const SIS_ROW = '%-10s%-9s%-8.2f%-12s %-4s  %1s %-30s';
    const SARC_ROW = '%-20s  %8.2f';
    const SIS_ITEM_TYPE = '251311003004';
    
    list($year, $month) = explode('-', date('y-m', strtotime('Last Month')));
    if((int)$month < 6) define('TERM', "1${year}2");
    elseif((int)$month < 8) define('TERM', "1${year}6");
    else define('TERM', "1${year}8");
    
    const ORIGIN_CODE = '00037';
    const MODULE = 'print_billing.php';
  
  
  $DEBUG = 0;
  
  
  
//    Process Options
      $options = array( 'log_path'        => '\\\\Administrative.arch.virginia.edu\\ADMINISTRATIVE\\NetAdmin\\scripts\\PHP\\Logs',
                        'user_map'        => '\\\\Administrative.arch.virginia.edu\\ADMINISTRATIVE\\NetAdmin\\Documentation\\Printing\\PfF\\list.ini',
                        'roster_file'     => '',
                        'output_path'     => getcwd(),
                        'email_warning'   => false,
                        'email_billing'   => false,
                        'action'          => '',
                        'cutoff'          => date('Y-m-d', strtotime('1 year ago')),
                        'sis_report'      => '',
                        'email_list'      => '',
                        'skip_csv'        => false,
                        'reversal'        => '',
                        'sis_file'        => array() );
      
      array_shift($argv);
      
      if(!count($argv)) {
      
        echo USAGE;
        die(1);
        
      }
      
      if(($action = strtoupper(array_shift($argv))) != 'IMPORT' && $action != 'EXPORT') {
        
        echo USAGE;
        die(2);
        
      }
      
      while($option = array_shift($argv)) {
        switch(($opt = strtoupper($option))) {
          case '--USER-MAP' :
            if(is_file($user_map = array_shift($argv))) {
              $options['user_map'] = $user_map;
            }
            break;
          
          case '--LOG-PATH' :
            if(is_dir($log_path = array_shift($argv))) {
              $options['log_path'] = $log_path;
            }
            //  Else Use Default
            break;
          
          case '--OUTPUT-PATH' :
            if(is_dir($output_path = array_shift($argv))) {
              $options['output_path'] = $output_path;
            }
            else die($output_path);
            break;

          case '--EMAIL-WARNING' :
            $options['email_warning'] = true;
            break;
          
          case '--EMAIL-BILLING' :
            $options['email_billing'] = true;
            break;
          
          case '--DEBUG' :
            $DEBUG = (int)array_shift($argv);
            break;
            
          case '--ITEMIZE' :
            $options['itemize'] = true;
            break;
            
          case '--SKIP-CSV' :
            $options['skip_csv'] = true;
            break;
            
          case '--SIS-REPORT' :
            if(is_file($sis_report = array_shift($argv))) {
              $options['sis_report'] = $sis_report;
            }
            break;
          
          case '--CUTOFF' :
            if(date('Y-m-d', strtotime($cutoff = array_shift($argv))) == $cutoff) $options['cutoff'] = $cutoff;
            break;
          
          case '--EMAIL-LIST' :
            $options['email_list'] = strtolower(preg_replace("/[^a-z0-9,]/i", '', array_shift($argv)));
            break;
            
          case '--REVERSAL' :
            $options['reversal'] = strtolower(preg_replace("/[^a-z0-9,]/i", '', array_shift($argv)));
            break;
            
          case '--SIS-FILE' :
            while($arg = array_shift($argv)) {
              if(!is_file($arg)) {
                array_unshift($argv, $arg);
                sort($options['sis_file'], SORT_STRING);
                break;
              }
              else {
                $options['sis_file'][] = $arg;
              }
            }
            break;
          
          default :
            die("\n'$opt' is not a valid option\n\n".USAGE);
            break;
        }
      }
      
      if($DEBUG > 2) print_r($options);


    
    
  
//  Open Log Files
    $LOGS = new LOGGING();
    
    try {
      
      $LOGS->set_Location('Process', LOG::FILE, "$options[log_path]\\PfF_" . date('Y-m-d') . '.log');
      $LOGS->set_Location('Error', LOG::FILE, "$options[log_path]\\PfF_error_" . date('Y-m-d') . '.log');
      if($DEBUG) $LOGS->set_Location('Debug', LOG::FILE, "$options[log_path]\\PfF_debug_" . date('Y-m-d') . '.log');
      
    }
    catch(Exception $e) {
      
      die("Error logging.\n\t" . $e->getMessage());
      
    }
    
    $LOGS->Process('(' . MODULE . ':' . __LINE__ . ') ' . "Running as user '" . get_current_user() . "' in directory '" . getcwd() . "'.");
    
    $filedate = $DEBUG ? date('Y-m-d(H-i)') : date('Ymd');
    
    $LOGS->Process('(' . MODULE . ':' . __LINE__ . ') ' . "${action}ing...");
    $LOGS->Process('(' . MODULE . ':' . __LINE__ . ') ' . "Running with log path '$options[log_path]'.");
    $LOGS->Process('(' . MODULE . ':' . __LINE__ . ') ' . "Running with output path '$options[output_path]'.");
    
    
    
//  Process Equitrac accounts and export reports
    if($action == 'EXPORT') {
    
      if($options['email_warning']) $LOGS->Process('(' . MODULE . ':' . __LINE__ . ') ' . "Running with 'Email Warning' turned on.");
      if($options['email_billing']) $LOGS->Process('(' . MODULE . ':' . __LINE__ . ') ' . "Running with 'Email Billing' turned on.");
      
//    Setup
//      Read User Map
        if(!($student_map = new USER_MAP($options['user_map'], $options['email_list']))) {
          //  ERROR CODE
          $LOGS->Error('(' . MODULE . ':' . __LINE__ . ') ' . "Failed to create new user map.");
          goto END;
        }
        $LOGS->Process('(' . MODULE . ':' . __LINE__ . ') ' . "Loaded user map file '$options[user_map]'.");
      
    
//    ReversalFile - Reversal of charges to student accounts. File uploaded to SIS SFTP site for processing. File format above.
      if($options['reversal'] != '') {
        if(count($options['sis_file']) == 0) {
        
          $LOGS->Error('(' . MODULE . ':' . __LINE__ . ') ' . "No SIS files found.");
          echo  "No SIS files found";
          goto END;
        
        }
        
        if(($ReversalFile = fopen("$options[output_path]/Reversal_$filedate.prn", 'w')) === false) {
      
          $LOGS->Error('(' . MODULE . ':' . __LINE__ . ') ' . "Failed to open Reversal File '$options[output_path]/Reversal_$filedate.prn'");
          goto END;
      
        }
        else {
      
          $LOGS->Process('(' . MODULE . ':' . __LINE__ . ') ' . "Opened Reversal File.");
      
        }
        
//      Read SIS File
        $total = $rowCount = 0;
        $SISFileData = '';
      
        foreach($options['sis_file'] as $file) {
          if(!($sis_file = file($file))) {
            //  ERROR CODE
            $LOGS->Error('(' . MODULE . ':' . __LINE__ . ') ' . "Failed to open SIS File.");
            goto END;
          }
          $LOGS->Process('(' . MODULE . ':' . __LINE__ . ') ' . "Loaded SIS File '$file'.");
        
  //      Create Reversal entry for each UID listed
          $CompIDs = explode(',', $options['reversal']);
          foreach($CompIDs as $CompID) {
        
            if($student = $student_map->get_user($CompID)) {
          
              $lines = preg_grep("/^$student[UID]/", $sis_file);
              if(count($lines)) {
                
                $line = array_shift($lines);
                $SISFileData .= substr_replace($line, 'Y', 46, 1);
            
                $total += (float)substr($line, 19, 8);
                $rowCount++;
                
              }
              
            }

          }
          
        }
        
        if(fwrite($ReversalFile, sprintf(SIS_HEADER, $rowCount, $total, ORIGIN_CODE) . "\r\n$SISFileData") === false) {
        
          $LOGS->Error('(' . MODULE . ':' . __LINE__ . ') ' . "Failed to write data to Reversal File");
        
        }
        fclose($ReversalFile);
        $LOGS->Process('(' . MODULE . ':' . __LINE__ . ') ' . "Closed SIS File.");
      
      }
        
      else {
  
//    Setup
//        Connec to Equitrac Database
          if(!($EQData = new EQUITRAC())) {
            //  ERROR CODE
            $LOGS->Error('(' . MODULE . ':' . __LINE__ . ') ' . "Failed to create new Equitrac connection.");
            goto END;
          }
          $LOGS->Process('(' . MODULE . ':' . __LINE__ . ') ' . "Connected to Equitrac database.");
        
        
//        Load Equitrac Data
          if(!($EQAccounts = $EQData->get_accounts(EQ_ACC_BALANCE | EQ_ACC_NEG_BAL))) {
            //  ERROR CODE
            $LOGS->Error('(' . MODULE . ':' . __LINE__ . ') ' . "Failed to load Equitrac accounts.");
            goto END;
          }
          $LOGS->Process('(' . MODULE . ':' . __LINE__ . ') ' . "Loaded Equitrac accounts.");
          if($DEBUG > 2) $LOGS->Debug('(' . MODULE . ':' . __LINE__ . ') ' . "Loaded Equitrac accounts..." . print_r($EQAccounts, true));
  


//      SARCRecord - Human readable account of charges
        if(($SARCRecord = fopen("$options[output_path]/SARCPrinting_$filedate.txt", 'w')) === false) {
        
          $LOGS->Error('(' . MODULE . ':' . __LINE__ . ') ' . "Failed to open SARC Record '$options[output_path]/SARCPrinting_$filedate.txt'");
          goto END;
        
        }
        else {
        
          $LOGS->Process('(' . MODULE . ':' . __LINE__ . ') ' . "Opened SARC Record.");
        
        }
        $SARCRecordChrg = '';
        $SARCRecordUnchrg = '';
      
    
//      SISFile - Charges to student accounts. File uploaded to SIS SFTP site for processing. File format above.
        if(($SISFile = fopen("$options[output_path]/SISPrinting_$filedate.prn", 'w')) === false) {
        
          $LOGS->Error('(' . MODULE . ':' . __LINE__ . ') ' . "Failed to open SIS File '$options[output_path]/SISPrinting_$filedate.prn'");
          goto END;
        
        }
        else {
        
          $LOGS->Process('(' . MODULE . ':' . __LINE__ . ') ' . "Opened SIS File.");
        
        }
        $SISFileData = '';
      
        
        $total = $rowCount = $totalUncharged = $rowCountUncharged = 0;
      
    
//      Setup message variables
        $SIS_Comment = 'SARC Printing ' . date('(Y F)', strtotime('Last Month'));
        $first = strtotime('first day of next month');
        $day = (int)date('w', $first);
        $nextWeekday  = $day == 0
                      ? '+2 days' :
                        $day > 0 && $day < 5
                      ? '+1 day' : '+3 days';
        $billing = strtotime($nextWeekday, $first);

//      Cycle through accounts and add to processing files if account has a UID and is in the negative
        foreach($EQAccounts as $compID=>$account) {
      
          //  If debugging only send 10 emails
          if($DEBUG && $rowCount > 9) continue;
        
        
          if((float)$account->balance < 0) $balance = -(float)$account->balance;
          else continue;
        
          //  Student in list
          if($options['email_list']!='' && !preg_match("/(^|,)$compID(,|$)/", $options['email_list'])) continue;
        
          if(!($student = $student_map->get_user($compID))) {
            //  No user map entry
                $SARCRecordUnchrg.= sprintf(SARC_ROW, $account->ID, $balance)."\r\n";
                $totalUncharged+= $balance;
                ++$rowCountUncharged;
          }
          elseif($student['LastUpdate'] < date('Y-m-d', strtotime($options['cutoff']))) continue;
          else {
            //  User map entry found
                $reversal = '';
                $SISFileData.= sprintf(SIS_ROW, $student['UID'], '', $balance, SIS_ITEM_TYPE, TERM, $reversal, $SIS_Comment)."\r\n";
                $SARCRecordChrg.= sprintf(SARC_ROW, $account->ID, $balance)."\r\n";
                //$EQBatchData.= sprintf(EQUITRAC_CHARGES, $account->ID, $balance)."\n";
                $total+= $balance;
                ++$rowCount;
              
                $emailTO  = "\"".preg_replace("/ *, */", ", ", $student['Name'])."\""
                          . ($DEBUG
                          ? " <db2d@virginia.edu>"
                          : " <$compID@virginia.edu>");
              
                if($options['email_warning'])
                  SARC_MAIL(array(  'Subject' => '[ACCOUNT INFO] SARC Printing System',
                                    'To'      => $emailTO,
                                    //'Cc'      => 'arch-request@virginia.edu',
                                    'Bcc'     => 'Dav Banks <davbanks@virginia.edu>',
                                    'From'    => 'SARC Help Desk<arch-request@virginia.edu>',
                                    'HTML'    => sprintf(SARC_EMAIL_WARNING_HTML, "eservices\\$compID", date('j M Y'), $balance, date('j M Y', $billing) ),
                                    'text'    => sprintf(SARC_EMAIL_WARNING_TEXT, "eservices\\$compID", date('j M Y'), $balance, date('j M Y', $billing) ) ) );
              
                if($options['email_billing'])
                  SARC_MAIL(array(  'Subject' => '[ACCOUNT INFO] SARC Printing System',
                                    'To'      => $emailTO,
                                    //'Cc'      => 'arch-request@virginia.edu',
                                    'Bcc'     => 'Dav Banks <davbanks@virginia.edu>',
                                    'From'    => 'SARC Help Desk<arch-request@virginia.edu>',
                                    'HTML'    => sprintf(SARC_EMAIL_BILLING_HTML, "eservices\\$compID", $balance, date('j M Y', strtotime('+1 day') ) ),
                                    'text'    => sprintf(SARC_EMAIL_BILLING_TEXT, "eservices\\$compID", $balance, date('j M Y', strtotime('+1 day') ) ) ) );
          }
        }
        $LOGS->Process('(' . MODULE . ':' . __LINE__ . ') ' . "Finished processing accounts.");
    
//      Write files
        if(fwrite($SARCRecord, "+------------------------------+\r\n"
                          . "| Billed to SIS Account        |\r\n"
                          . "| Row Count: " . str_pad((string)$rowCount, 17, ' ', STR_PAD_LEFT) . " |\r\n"
                          . "| Adjustment Total: " . str_pad($total, 10, ' ', STR_PAD_LEFT) . " |\r\n"
                          . "+------------------------------+\r\n$SARCRecordChrg\r\n\r\n"
                          . "+------------------------------+\r\n"
                          . "| NOT BILLED                   |\r\n"
                          . "| Row Count: " . str_pad((string)$rowCountUncharged, 17, ' ', STR_PAD_LEFT) . " |\r\n"
                          . "| Adjustment Total: " . str_pad($totalUncharged, 10, ' ', STR_PAD_LEFT) . " |\r\n"
                          . "+------------------------------+\r\n$SARCRecordUnchrg") === false) {
        
          $LOGS->Error('(' . MODULE . ':' . __LINE__ . ') ' . "Failed to write data to SARC Record");
        
        }
        fclose($SARCRecord);
        $LOGS->Process('(' . MODULE . ':' . __LINE__ . ') ' . "Closed SARC Record.");
      
        if(fwrite($SISFile, sprintf(SIS_HEADER, $rowCount, $total, ORIGIN_CODE) . "\r\n$SISFileData") === false) {
        
          $LOGS->Error('(' . MODULE . ':' . __LINE__ . ') ' . "Failed to write data to SIS File");
        
        }
        fclose($SISFile);
        $LOGS->Process('(' . MODULE . ':' . __LINE__ . ') ' . "Closed SIS File.");
      
      }
      
    }
    


//  Process SIS Report and update Equitrac accounts
    else {
        
      $LOGS->Process('(' . MODULE . ':' . __LINE__ . ') ' . "Running with SIS Report '$options[sis_report]'.");
      
//    EQBatch - Adjustments to Equitrac accounts. File processed by EQCmd
      if(is_file("$options[output_path]/EQBatch_$filedate.csv")) {
        
        //  CSV Exists
        $LOGS->Error('(' . MODULE . ':' . __LINE__ . ') ' . "Equitrac Batch '$options[output_path]/EQBatch_$filedate.csv' already exists.");
        goto END;
        
      }
      if(!($EQBatch = fopen("$options[output_path]/EQBatch_$filedate.csv", 'w'))) {
        
        //  ERROR CODE
        $LOGS->Error('(' . MODULE . ':' . __LINE__ . ') ' . "Failed to open Equitrac Batch '$options[output_path]/EQBatch_$filedate.csv'.");
        goto END;
        
      }
      $EQBatchData = '';
      $LOGS->Process('(' . MODULE . ':' . __LINE__ . ') ' . "Open Equitrac Batch '$options[output_path]/EQBatch_$filedate.csv'.");
      
      
//    Connec to Equitrac Database
      if(!($EQData = new EQUITRAC())) {
      
        //  ERROR CODE
        $LOGS->Error('(' . MODULE . ':' . __LINE__ . ') ' . "Failed to create new Equitrac connection.");
        goto END;
        
      }
      $LOGS->Process('(' . MODULE . ':' . __LINE__ . ') ' . "Connected to Equitrac database.");
      
      
//    Load Equitrac Data
      if(!($EQAccounts = $EQData->get_accounts(EQ_ACC_BALANCE | EQ_ACC_NEG_BAL))) {
      
        //  ERROR CODE
        $LOGS->Error('(' . MODULE . ':' . __LINE__ . ') ' . "Failed to load Equitrac accounts.");
        goto END;
      }
      $LOGS->Process('(' . MODULE . ':' . __LINE__ . ') ' . "Loaded Equitrac accounts.");
      if($DEBUG > 2) $LOGS->Debug('(' . MODULE . ':' . __LINE__ . ') ' . "Loaded Equitrac accounts..." . print_r($EQAccounts, true));
      
      

      $charge_lines = PARSEFILE::parse(array( 'File'        => $options["sis_report"],
                                              'Column-Line' => true));
      
      foreach($charge_lines as $charge) {
        if($charge['Student:Computing ID'] != '') fwrite($EQBatch, sprintf(EQUITRAC_CHARGES, "eservices\\".$charge['Student:Computing ID'], $charge['Item:Amount'])."\n");
      }
      foreach($EQAccounts as $account) {
        if($account->limit != -500) fwrite($EQBatch, sprintf(EQUITRAC_MINIMUM, $account->ID)."\n");
      }
      fclose($EQBatch);
      

//    Execute Batch
      if(!$options['skip_csv']) {
      
        $output = `eqcmd -sprint.arch.virginia.edu -f"$options[output_path]\\EQBatch_$filedate.csv"`;
      
        $log_lines = file("$options[output_path]\\EQBatch_$filedate.csv.log", FILE_IGNORE_NEW_LINES || FILE_SKIP_EMPTY_LINES);
        array_shift($log_lines);
        foreach($log_lines as $log_line) {
          
          if(strpos($log_line, 'command processed successfully') === false) $LOGS->Error('(' . MODULE . ':' . __LINE__ . ') ' . $log_line);
          
        }
        
      }

    }
    

  
      
      
      
      
//  Clean Up
    END:
//    Close Log Files
      $LOGS->Process('(' . MODULE . ':' . __LINE__ . ') ' . "Closing Logs.");
      $LOGS->close();
      

?>