<?php

  //  Usage text
  const USAGE = <<<EOT
  
usage: print_billing.php <options>
  --log-path {directory}      Location to write log files
  --user-map {file}           .ini file with UVa Computing ID to University ID mapping
  --output-path {directory}   Location to write output files (SIS upload, SARC record, Equitrac adjustments)
  --email-warning             Issues 10 day warning to all users with negative balances to check account
  --email-billing             Emails summary of SIS charges
  --export-files              Exports the SIS billing and Equitrac accounting files in addition to a tracking file
  --sis-report {file}         Tab delimited SIS billing report for a given date. Used to generate account adjustments in Equitrac
  --cutoff {date}             If student map entry is older than this date, do not bill. FORMAT: YYYY-MM-DD
  --debug                     Testing

EOT;


  //  Warning Email (HTML) - Notify account holders that they have 10 days to reconcile their accounts before billing
  const SARC_EMAIL_WARNING_HTML = <<<'EOT'
<html>
  <head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
    <style>
      BODY {
        font-family: 'TradeGothicW02-BoldCn20675505',sans-serif!important;
        font-weight: 400!important;
        color: #333;
        text-decoration: none;
    </style>
  </head>
  
  <body>
    <table style='width: 500px; border: 1px solid #dedede'>
      <tr>
        <td colspan=4><a href='http://www.arch.virginia.edu/' title='Home' rel='home'><img src='http://www.arch.virginia.edu/sites/all/themes/aschool/img/header/logo.jpg'></a></td>
      </tr>
      
      <tr>
        <td colspan=4 style='padding-bottom: 10px; text-align: center;'><span style='font-size: 17px;padding: 6px 30px 6px 0px;text-transform: uppercase;text-decoration: underline;'>SARC Print Billing</span></td>
      </tr>
      
      <tr>
        <td style='vertical-align: top; width: 60px;'><b>Account:</b></td>
        <td style='padding-bottom: 10px;'>eservices\\$options[CompID]</td>
        <td style='vertical-align: top; width: 60px;'><b>Today:</b></td>
        <td style='padding-bottom: 10px;'>".date('j M Y')."</td>
      </tr>
      
      <tr>
        <td style='vertical-align: top;'><b>Balance:</b></td>
        <td style='padding-bottom: 40px;'>\$$options[Balance]</td>
        <td style='vertical-align: top; width: 60px;'><b>Bill On:</b></td>
        <td style='padding-bottom: 40px;'>".date('j M Y', $billing)."</td>
      </tr>
      
      <tr>
        <td colspan=4>NOTES<br />
          <ol>
            <li>This message is a reminder to monitor your printing balance<br />
              On the first day of next month, you will see a bill for your printing.<br />
              The bill will include all printing charges up through the end of this month (the balance above, plus any new charges)<br />
              Bills will be charged to your Student Financial Services account (the same as where you pay for tuition).
            </li><br />
            <li><a href='http://print.arch.virginia.edu:2941/webtools/'>View your statement</a>
                  Please view your printing statement, and monitor your printing balance <a href='http://print.arch.virginia.edu:2941/webtools/'>here</a>.<br />
                  If you have any charges that need attention, please bring them to the Business Office<br />
                  <blockquote>
                    &ndash; <a href='http://www.arch.virginia.edu/people/directory/lisa-benton'>Lisa Benton</a>, room 211<br />
                    &ndash; <a href='http://www.arch.virginia.edu/people/directory/leslie-fitzgerald'>Leslie Fitzgerald</a>, room 227
                  </blockquote>
                  Bring with you a printout of your statement (use the link above to get this), with bad charges <span style='background-color: #ffff40'>highlighted</span>.<br /><br />                  
                  <a href='http://www.virginia.edu/studentaccounts/quikpay.html'>QuickPay</a> &mdash; view and pay your Student Financial Services Account
            </li>
          </ol>
        </td>
      </tr>
      
      <tr>
        <td colspan=4>
          <img src='http://www.arch.virginia.edu/sites/all/themes/aschool/img/rotunda.gif' style='left: 0px; bottom: 0px;'>
          <p style='color: #dedede; text-align: center;'>&#169; Copyright 2012 University of Virginia School of Architecture</p>
        </td>
      </tr>
      
    </table>
  </body>
</html>
EOT;


  //  Warning Email (TEXT) - Notify account holders that they have 10 days to reconcile their accounts before billing
  const SARC_EMAIL_WARNING_TEXT = <<<'EOT'
University of Virginia
School of Architecture

SARC PRINT BILLING
Account:	eservices\\$options[CompID]
Date:	".date('j M Y')."
Balance:	\$$options[Balance]
Bill On:	".date('j M Y', $billing)."

NOTES
This message is a reminder to monitor your printing balance
On the first day of next month, you will see a bill for your printing.
The bill will include all printing charges up through the end of this month (the balance above, plus any new charges)
Bills will be charged to your Student Financial Services account (the same as where you pay for tuition).

View your statement (http://print.arch.virginia.edu:2941/webtools/)
Please view your printing statement, and monitor your printing balance here.
If you have any charges that need attention, please bring them to the Business Office
– Lisa Benton, room 211
– Leslie Fitzgerald, room 227
Bring with you a printout of your statement (use the link above to get this), with bad charges highlighted.

QuickPay (http://www.virginia.edu/studentaccounts/quikpay.html)
view and pay your Student Financial Services Account
EOT;


  //  Billing Email (HTML) - Notify account holders of billing
  const SARC_EMAIL_BILLING_HTML = <<<'EOT'
<html>
  <head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
    <style>
      BODY {
        font-family: 'TradeGothicW02-BoldCn20675505',sans-serif!important;
        font-weight: 400!important;
        color: #333;
        text-decoration: none;
    </style>
  </head>
  
  <body>
    <table style='width: 500px; border: 1px solid #dedede'>
      <tr>
        <td colspan=4><a href='http://www.arch.virginia.edu/' title='Home' rel='home'><img src='http://www.arch.virginia.edu/sites/all/themes/aschool/img/header/logo.jpg'></a></td>
      </tr>
      
      <tr>
        <td colspan=4 style='padding-bottom: 10px; text-align: center;'><span style='font-size: 17px;padding: 6px 30px 6px 0px;text-transform: uppercase;text-decoration: underline;'>SARC Print Billing</span></td>
      </tr>
      
      <tr>
        <td style='vertical-align: top; width: 60px;'><b>Account:</b></td>
        <td colspan=3 style='padding-bottom: 10px;'>eservices\\$options[CompID]</td>
      </tr>
      
      <tr>
        <td style='vertical-align: top;'><b>Billed:</b></td>
        <td style='padding-bottom: 40px;'>\$$options[Balance]</td>
        <td style='vertical-align: top; width: 60px;'><b>Billed On:</b></td>
        <td style='padding-bottom: 40px;'>".date('j M Y', $billing)."</td>
      </tr>
      
      <tr>
        <td colspan=4>Use <a href='http://www.virginia.edu/studentaccounts/quikpay.html'>QuickPay</a> to view and pay your Student Financial Services Account<br /><br /><br /><br /></td>
      </tr>
      
      <tr>
        <td colspan=4>
          <img src='http://www.arch.virginia.edu/sites/all/themes/aschool/img/rotunda.gif' style='left: 0px; bottom: 0px;'>
          <p style='color: #dedede; text-align: center;'>&#169; Copyright 2012 University of Virginia School of Architecture</p>
        </td>
      </tr>
      
    </table>
  </body>
</html>
EOT;


  //  Billing Email (TEXT) - Notify account holders of billing
  const SARC_EMAIL_BILLING_TEXT = <<<'EOT'
University of Virginia
School of Architecture

SARC PRINT BILLING
Account:	eservices\\$options[CompID]
Billed:	\$$options[Balance]
Billed On:	".date('j M Y', $billing)."

Use QuickPay (http://www.virginia.edu/studentaccounts/quikpay.html)
to view and pay your Student Financial Services Account
EOT;


?>