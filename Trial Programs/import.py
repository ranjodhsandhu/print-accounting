from bs4 import BeautifulSoup
import sys
import csv
import requests
import os
import operator

urlc = "https://plotc.arch.virginia.edu/hp/device/webAccess/accounting.xls"
urld = "https://plotd.arch.virginia.edu/hp/device/webAccess/accounting.xls"


#IMPORT PLOT C DATA AS XML
r = requests.get(urlc, allow_redirects=True, verify=False)
open("data.xml", "wb").write(r.content)

#CONVERT XML TO A TEMP CSV
html = open("data.xml")
soup = BeautifulSoup(html, "lxml")
table = soup.find("table")
output_rows = []
for table_row in table.findAll('tr'):
    columns = table_row.findAll('td')
    output_row = []
    for column in columns:
        output_row.append(column.text)
    output_rows.append(output_row)   
with open('PlotCdata.csv', 'w') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerows(output_rows)

#ADD TO ANOTHER TEMP CSV BUT WITH APPENDED PLOT C COLUMN
with open('PlotCdata.csv','r') as csvinput:
    with open('data1.csv', 'w') as csvoutput:
        writer = csv.writer(csvoutput, lineterminator='\n')
        reader = csv.reader(csvinput)
        all = []
        row = next(reader)
        all.append(row)
        for row in reader:
            row.append('Plot C')
            all.append(row)
        writer.writerows(all)



#IMPORT PLOT D DATA AS XML
r = requests.get(urld, allow_redirects=True, verify=False)
open("data.xml", "wb").write(r.content)

#CONVERT XML TO TEMP CSV
html = open("data.xml")
soup = BeautifulSoup(html, "lxml")
table = soup.find("table")
output_rows = []
for table_row in table.findAll('tr'):
    columns = table_row.findAll('td')
    output_row = []
    for column in columns:
        output_row.append(column.text)
    output_rows.append(output_row)    
with open('PlotDdata.csv', 'w') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerows(output_rows)

#MERGE WITH TEMP CSV WITH PLOT C COLUMNS AND APPEND PLOT D
with open('PlotDdata.csv','r') as csvinput:
    with open('data1.csv', 'a') as csvoutput:
        writer = csv.writer(csvoutput, lineterminator='\n')
        reader = csv.reader(csvinput)
        all = []
        row = next(reader)
        all.append(row)
        for row in reader:
            row.append('Plot D')
            all.append(row)
        writer.writerows(all)

#REMOVE BLANK LINES INTO ANOTHER TEMP CSV FILE
with open('data1.csv') as in_file:
    with open('data2.csv', 'w') as out_file:
        writer = csv.writer(out_file)
        for row in csv.reader(in_file):
            if row:
                writer.writerow(row)

#REMOVE EXTRA DATA
with open("data3.csv","r") as source:
    rdr= csv.reader( source )
    with open("data.csv","w") as result:
        wtr= csv.writer( result )
        for r in rdr:
            wtr.writerow( (r[0], r[2], r[4], r[5], r[6], r[8], r[9], r[12]) )


#SORT BY DATE
reader = csv.reader(open("data2.csv"))
sortedlist = sorted(reader, key=lambda row: row[9], reverse=True)
writer = csv.writer(open('dataOut.csv', 'w'), lineterminator='\n')
writer.writerows(sortedlist)

#DELETE TEMP FILES
os.remove("PlotCdata.csv")
os.remove("PlotDdata.csv")
os.remove("data1.csv")
os.remove("data2.csv")
