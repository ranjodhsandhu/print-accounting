import string
from openpyxl import load_workbook
path = '/home/ranjodhsandhu/Documents/print-accounting/test1.xlsx'
wb = load_workbook(path)
ws = wb.active
string = raw_input("Enter Computing ID: ")
search_words = [string]

def print_row(row):
    line = ''
    for col in xrange(1,ws.max_column + 1):
        _cell = ws.cell(row=row, column=col).value
        if _cell:
            line += ' ' + str(_cell)
    print(line)

for row in xrange(1,ws.max_row + 1):
    for col in xrange(1,ws.max_column + 1):
        _cell = ws.cell(row=row, column=col)
        if any(word in str(_cell.value) for word in search_words):
            print_row(row)
            break

