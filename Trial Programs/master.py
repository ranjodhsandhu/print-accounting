from bs4 import BeautifulSoup
import csv
import requests
url = "https://plotd.arch.virginia.edu/hp/device/webAccess/accounting.xls"
r = requests.get(url, allow_redirects=True, verify=False)
open("data.xml", "wb").write(r.content)
html = open("data.xml")
soup = BeautifulSoup(html, "lxml")
table = soup.find("table")
output_rows = []
for table_row in table.findAll('tr'):
    columns = table_row.findAll('td')
    output_row = []
    for column in columns:
        output_row.append(column.text)
    output_rows.append(output_row)
    
with open('test.csv', 'w') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerows(output_rows)



def searchfile(string):
    search_words = [string]
    with open('/home/ranjodhsandhu/Documents/print-accounting/test.csv') as f:
        reader = csv.reader(f)
        for row in reader:
            print(row)
            break
        for row in reader:
            if string in row:
                print(row[8] + " printed " + '"' + row[0] + '"' + " on "+ row[9] + " using " + row[6] + " mL of ink and " + row[5] + " square feet of paper.")

while True:
    string = input("Enter User Name: ")
    if string == "":
        break
    searchfile(string)


#import xml.etree.ElementTree as ET 
#import json
#import xmltodict
#tree = ET.parse("data.xml")
#root = tree.getroot()
#xmlstr = ET.tostring(root)
#data_dict = dict(xmltodict.parse(xmlstr))
#csv_file = 'Data.csv'
#csv_columns = ['Document','Job type','Job source','Job output','Status','Copies','Paper type','Paper usage ft','Paper length','Scan length','Mono category','Ink used ml','User name','Printing time','Print quality','Account ID']
#try:
#    with open(csv_file, 'w') as csvfile:
#        writer = csv.DictWriter(csvfile, fieldnames= csv_columns)
#        writer.writeheader()
#        for data in data_dict:
#            writer.writerow(data)
#except IOError:
#    print("I/O error") 