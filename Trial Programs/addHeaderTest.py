from bs4 import BeautifulSoup
import sys
import csv
import requests
import os
import operator
from datetime import datetime
"""
reader = csv.reader(open("dataOut.csv"))
sortedlist = sorted(reader, key = lambda row: datetime.strptime(row[6], '%b %d, %Y %I:%M:%S %p'), reverse=True)

f = open("data.csv", "w")
writer = csv.DictWriter(f, fieldnames=['Document', 'Status', 'Paper Type', 'Paper Usage (sq. ft)', 'Ink Usage (mL)', 'User ID', 'Date', 'Printer Plot'])
writer.writeheader()
f.close()

writerows = csv.writer(open('data.csv', 'a'), lineterminator='\n')
writerows.writerows(sortedlist)
"""
reader = csv.reader(open("data.csv"))
sortedlist = sorted(reader, key = lambda row: datetime.strptime(row[6], '%b %d, %Y %I:%M:%S %p'), reverse=True)
writer = csv.writer(open('dataOut.csv', 'w'), lineterminator='\n')
writer.writerow(['Document', 'Status', 'Paper Type', 'Paper Usage (sq. ft)', 'Ink Usage (mL)', 'UserID', 'Date', 'Printer Plot'])
writer.writerows(sortedlist)