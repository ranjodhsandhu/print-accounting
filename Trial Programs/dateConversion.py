import csv
import sys
from datetime import datetime

"""
FMT = '%b %d, %Y %I:%M:%S %p'

with open('data.csv', 'r') as in_file:
    reader = csv.reader(in_file)
    with open("dataOut.csv","w") as result:
        wtr= csv.writer( result )
        for row in reader:
            dt = datetime.strptime(row[6], FMT)
            wtr.writerow((row[0], row[1], row[2], row[3], row[4], row[5], dt, row[7]))
            """
reader = csv.reader(open("data.csv"))
sortedlist = sorted(reader, key = lambda row: datetime.strptime(row[6], '%b %d, %Y %I:%M:%S %p'), reverse=True)
writer = csv.writer(open('dataOut.csv', 'w'), lineterminator='\n')
writer.writerows(sortedlist)
