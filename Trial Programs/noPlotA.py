from bs4 import BeautifulSoup
import sys
import csv
import requests
import os
import operator

#urla = "https://plota.arch.virginia.edu/hp/device/webAccess/accounting.xls"
urlb = "https://plotb.arch.virginia.edu/hp/device/webAccess/accounting.xls"

'''
r = requests.get(urla, allow_redirects=True, verify=False)
open("data.xml", "wb").write(r.content)

#CONVERT XML TO A TEMP CSV
html = open("data.xml")
soup = BeautifulSoup(html, "lxml")
table = soup.find("table")
output_rows = []
for table_row in table.findAll('tr'):
    columns = table_row.findAll('td')
    output_row = []
    for column in columns:
        output_row.append(column.text)
    output_rows.append(output_row)   
with open('PlotAdata.csv', 'w') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerows(output_rows)

#ADD TO ANOTHER TEMP CSV BUT WITH APPENDED PLOT A COLUMN
with open('PlotAdata.csv','r') as csvinput:
    with open('data1.csv', 'w') as csvoutput:
        writer = csv.writer(csvoutput, lineterminator='\n')
        reader = csv.reader(csvinput)
        all = []
        row = next(reader)
        all.append(row)
        for row in reader:
            row.append('Plot A')
            all.append(row)
        writer.writerows(all)
'''
#IMPORT PLOT B DATA AS XML
r = requests.get(urlb, allow_redirects=True, verify=False)
open("data.xml", "wb").write(r.content)

#CONVERT XML TO A TEMP CSV
html = open("data.xml")
soup = BeautifulSoup(html, "lxml")
table = soup.find("table")
output_rows = []
for table_row in table.findAll('tr'):
    columns = table_row.findAll('td')
    output_row = []
    for column in columns:
        output_row.append(column.text)
    output_rows.append(output_row)    
with open('PlotBdata.csv', 'w') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerows(output_rows)

#MERGE WITH TEMP CSV WITH PLOT A COLUMNS AND APPEND PLOT B
with open('PlotBdata.csv','r') as csvinput:
    with open('data1.csv', 'w') as csvoutput:
        writer = csv.writer(csvoutput, lineterminator='\n')
        reader = csv.reader(csvinput)
        all = []
        row = next(reader)
        all.append(row)
        for row in reader:
            row.append('Plot B')
            all.append(row)
        writer.writerows(all)

#REMOVE BLANK LINES INTO ANOTHER TEMP CSV FILE
with open('data1.csv') as in_file:
    next(in_file)
    next(in_file)
    next(in_file)
    with open('data2.csv', 'w') as out_file:
        writer = csv.writer(out_file)
        for row in csv.reader(in_file):
            if row:
                writer.writerow(row)
#REMOVE EXTRA DATA
with open("data2.csv","r") as source:
    rdr= csv.reader( source )
    with open("data.csv","w") as result:
        wtr= csv.writer( result )
        for r in rdr:
            wtr.writerow( (r[0], r[4], r[6], r[7], r[11], r[12], r[13], r[16]))