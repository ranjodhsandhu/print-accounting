from bs4 import BeautifulSoup
from pytz import timezone
import sys
import csv
import requests
import os
import operator
from datetime import datetime
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

with open('data.csv', "r") as source:
    rdr = csv.reader(source)
    next(rdr)
    with open('dataOut', 'w') as dest:
        writer = csv.writer(dest)
        for r in rdr:
            print(r[6])
            date = datetime.strptime(r[6], '%b %d, %Y %I:%M:%S %p')
            print(date)
            gmtdate = timezone('GMT').localize(date)
            print(gmtdate)
            estdate = gmtdate.astimezone(timezone('EST'))
            print(estdate)
            converted = datetime.strftime(estdate, '%b %d, %Y %I:%M:%S %p')
            print(converted)
            break
            writer.writerow((r[0], r[1], r[2], r[3], r[4], r[5], converted, r[7]))