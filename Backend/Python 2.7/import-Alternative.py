from bs4 import BeautifulSoup
from pytz import timezone
import sys
import csv
import requests
import os
import operator
import time
import urllib
#import pandas as pd
from datetime import datetime
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

#Parameters
plotAEnabled = True
plotBEnabled = True
plotCEnabled = True
plotDEnabled = True

urla = "https://plota.arch.virginia.edu/hp/device/webAccess/accounting.xls"
urlb = "https://plotb.arch.virginia.edu/hp/device/webAccess/accounting.xls"
urlc = "https://plotc.arch.virginia.edu/hp/device/webAccess/accounting.xls"
urld = "https://plotd.arch.virginia.edu/hp/device/webAccess/accounting.xls"


def fetchXML(url):
    r = requests.get(url, allow_redirects=True, verify=False)
    open("data.xml", "wb").write(r.content)
    return None

def convertXML(fileName):
    html = open("data.xml")
    soup = BeautifulSoup(html, "lxml")
    table = soup.find("table")
    output_rows = []
    for table_row in table.findAll('tr'):
        columns = table_row.findAll('td')
        output_row = []
        for column in columns:
            output_row.append(column.text)
        output_rows.append(output_row)   
    with open(fileName, 'w') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerows(output_rows)
    return None

def appendColumn(inputFile, outputFile, plotName):
    with open(inputFile,'r') as csvinput:
        with open(outputFile, 'w') as csvoutput:
            writer = csv.writer(csvoutput, lineterminator='\n')
            reader = csv.reader(csvinput)
            all = []
            row = next(reader)
            all.append(row)
            for row in reader:
                row.append(plotName)
                all.append(row)
            writer.writerows(all)
    return None

def merge(input1, input2):
    with open(input2,'r') as csvinput:
        with open(input1, 'a') as csvoutput:
            writer = csv.writer(csvoutput, lineterminator='\n')
            reader = csv.reader(csvinput)
            all = []
            row = next(reader)
            all.append(row)
            for row in reader:
                all.append(row)
            writer.writerows(all)
    return None

def removeExtraDataAB(input, output):
    with open(input,"r") as source:
        rdr= csv.reader( source )
        with open(output,"w") as result:
            wtr= csv.writer( result )
            try:
                for r in rdr:
                    if r:
                        wtr.writerow( (r[0], r[4], r[6], r[7], r[11], r[12], r[13], r[16]))
            except IndexError:
                next(rdr)
                for r in rdr:
                    if r:
                        wtr.writerow( (r[0], r[4], r[6], r[7], r[11], r[12], r[13], r[16]))
    return None

def removeExtraDataCD(input, output):
    with open(input,"r") as source:
        rdr= csv.reader( source )
        with open(output,"w") as result:
            wtr= csv.writer( result )
            try:
                for r in rdr:
                    if r:
                        wtr.writerow( (r[0], r[2], r[4], r[5], r[6], r[8], r[9], r[12]) )
            except IndexError:
                next(rdr)
                for r in rdr:
                    if r:
                        wtr.writerow( (r[0], r[2], r[4], r[5], r[6], r[8], r[9], r[12]) )
    return None

#def removeDuplicates(file):
 #   df = pd.read_csv(file)
  #  df.drop_duplicates(inplace=True)
   # df.to_csv(file, index=False)
def removeDuplicates(file):
    os.remove('dataOut.csv')
    with open(file,'r') as in_file, open('dataOut.csv','w') as out_file:
        seen = set() # set for fast O(1) amortized lookup
        for line in in_file:
            if line in seen: continue # skip duplicate
            seen.add(line)
            out_file.write(line)
    os.remove(file)
    os.rename('dataOut.csv', file)

def sortandAppend(file):
    reader = csv.reader(open(file), lineterminator='\n')
    next(reader)
    sortedlist = sorted(reader, key = lambda row: datetime.strptime(row[6], '%b %d, %Y %I:%M:%S %p'), reverse=True)
    writer = csv.writer(open('dataOut.csv', 'w'))
    writer.writerow(['Document', 'Status', 'Paper Type', 'Paper Usage (sq. ft)', 'Ink Usage (mL)', 'UserID', 'Date', 'Printer Plot'])
    writer.writerows(sortedlist)
    os.remove(file)
    os.rename('dataOut.csv', file)
    return None

def headerAndAppend():
    reader = csv.reader(open("dataOut.csv"))
    filename = 'data.csv'
    if os.path.exists(filename):
        flag = 'a'
    else:
        flag = 'w'
    writer = csv.writer(open(filename, flag), lineterminator='\n')
    for line in reader:
        writer.writerow(line)
    return None

def dateConversion(input, output):
    with open(input, "r") as source:
        rdr = csv.reader(source)
        with open(output, 'w') as dest:
            writer = csv.writer(dest)
            for r in rdr:
                date = datetime.strptime(r[6], '%b %d, %Y %I:%M:%S %p')
                gmtdate = timezone('GMT').localize(date)
                estdate = gmtdate.astimezone(timezone('US/Eastern'))
                converted = datetime.strftime(estdate, '%b %d, %Y %I:%M:%S %p')
                writer.writerow((r[0], r[1], r[2], r[3], r[4], r[5], converted, r[7]))

def clean():
    os.remove("PlotAdata.csv")
    os.remove("PlotBdata.csv")
    os.remove("PlotCdata.csv")
    os.remove("PlotDdata.csv")
    os.remove("data1.csv")
    os.remove("data2.csv")
    os.remove("data3.csv")
    os.remove("data4.csv")
    os.remove("data5.csv")
    os.remove("data6.csv")
    os.remove("data7.csv")
    os.remove('data8.csv')
    os.remove('data.xml')
    return None

def main():
    if(plotAEnabled == True):
        fetchXML(urla)
        convertXML('PlotAdata.csv')
        row_countA = sum(1 for row in csv.reader(open('PlotAdata.csv')))
        if row_countA < 2:
            urlWA = 'http://plota.arch.virginia.edu/wakeup.htm/config?hidden_wakeup_Addr=""&WakeUp=Wake\ Up'
            urllib.urlopen(urlWA)
            print('Waking Up Plot A')
            time.sleep(30)
            main()
        appendColumn('PlotAdata.csv', 'data1.csv', 'Plot A')
    else:
        with open('data1.csv', 'w') as input:
            pass
        with open('PlotAdata.csv', 'w') as input:
            pass

    if(plotBEnabled == True):
        fetchXML(urlb)
        convertXML('PlotBdata.csv')
        row_countB = sum(1 for row in csv.reader(open('PlotBdata.csv')))
        if row_countB < 2:
            urlWB = 'http://plotb.arch.virginia.edu/wakeup.htm/config?hidden_wakeup_Addr=""&WakeUp=Wake\ Up'
            urllib.urlopen(urlWB)
            print('Waking Up Plot B')
            time.sleep(30)
            main()
        appendColumn('PlotBdata.csv', 'data2.csv', 'Plot B')
    else:
        with open('data2.csv', 'w') as input:
            pass
        with open('PlotBdata.csv', 'w') as input:
            pass

    if(plotCEnabled == True):
        fetchXML(urlc)
        convertXML('PlotCdata.csv')
        appendColumn('PlotCdata.csv', 'data3.csv', 'Plot C')
    else:
        with open('data3.csv', 'w') as input:
            pass
        with open('PlotCdata.csv', 'w') as input:
            pass

    if(plotDEnabled == True):
        fetchXML(urld)
        convertXML('PlotDdata.csv')
        appendColumn('PlotDdata.csv', 'data4.csv', 'Plot D')
    else:
        with open('data4.csv', 'w') as input:
            pass
        with open('PlotDdata.csv', 'w') as input:
            pass
    removeExtraDataAB('data1.csv', 'dataOut.csv')
    removeExtraDataAB('data2.csv', 'data5.csv')
    merge('dataOut.csv', 'data5.csv')

    removeExtraDataCD('data3.csv', 'data6.csv')
    removeExtraDataCD('data4.csv', 'data7.csv')
    merge('data6.csv', 'data7.csv')
    dateConversion('data6.csv', 'data8.csv')

    merge('dataOut.csv', 'data8.csv')
    headerAndAppend()
    removeDuplicates('data.csv')
    sortandAppend('data.csv')
    clean()
main()