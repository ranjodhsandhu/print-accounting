import csv
import pandas as pd
def removeDuplicates(file):
    df = pd.read_csv(file)
    df.drop_duplicates(inplace=True)
    df.to_csv(file, index=False)

removeDuplicates('test.csv')