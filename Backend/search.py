import csv
def searchfile(string):
    search_words = [string]
    with open('data.csv') as f:
        reader = csv.reader(f)
        for row in reader:
            if string in row:
                print(row[5] + " printed " + '"' + row[0] + '"' + " from "+ row[7] + " on "+ row[6] + " using " + row[4] + " mL of ink and " + row[3] + " square feet of paper.")

while True:
    string = input("Enter User Name: ")
    if string == "":
        break
    searchfile(string)
