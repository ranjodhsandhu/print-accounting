from bs4 import BeautifulSoup
import sys
import csv
import requests
import os
import operator
from datetime import datetime

urla = "https://plota.arch.virginia.edu/hp/device/webAccess/accounting.xls"
urlb = "https://plotb.arch.virginia.edu/hp/device/webAccess/accounting.xls"
urlc = "https://plotc.arch.virginia.edu/hp/device/webAccess/accounting.xls"
urld = "https://plotd.arch.virginia.edu/hp/device/webAccess/accounting.xls"

#IMPORT PLOT A DATA AS XML
r = requests.get(urla, allow_redirects=True, verify=False)
open("data.xml", "wb").write(r.content)

#CONVERT XML TO A TEMP CSV
html = open("data.xml")
soup = BeautifulSoup(html, "lxml")
table = soup.find("table")
output_rows = []
for table_row in table.findAll('tr'):
    columns = table_row.findAll('td')
    output_row = []
    for column in columns:
        output_row.append(column.text)
    output_rows.append(output_row)   
with open('PlotAdata.csv', 'w') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerows(output_rows)

#ADD TO ANOTHER TEMP CSV BUT WITH APPENDED PLOT A COLUMN
with open('PlotAdata.csv','r') as csvinput:
    with open('data1.csv', 'w') as csvoutput:
        writer = csv.writer(csvoutput, lineterminator='\n')
        reader = csv.reader(csvinput)
        all = []
        row = next(reader)
        all.append(row)
        for row in reader:
            row.append('Plot A')
            all.append(row)
        writer.writerows(all)

#IMPORT PLOT B DATA AS XML
r = requests.get(urlb, allow_redirects=True, verify=False)
open("data.xml", "wb").write(r.content)

#CONVERT XML TO A TEMP CSV
html = open("data.xml")
soup = BeautifulSoup(html, "lxml")
table = soup.find("table")
output_rows = []
for table_row in table.findAll('tr'):
    columns = table_row.findAll('td')
    output_row = []
    for column in columns:
        output_row.append(column.text)
    output_rows.append(output_row)    
with open('PlotBdata.csv', 'w') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerows(output_rows)

#MERGE WITH TEMP CSV WITH PLOT A COLUMNS AND APPEND PLOT B
with open('PlotBdata.csv','r') as csvinput:
    with open('data1.csv', 'a') as csvoutput:
        writer = csv.writer(csvoutput, lineterminator='\n')
        reader = csv.reader(csvinput)
        all = []
        row = next(reader)
        all.append(row)
        for row in reader:
            row.append('Plot B')
            all.append(row)
        writer.writerows(all)

#REMOVE BLANK LINES INTO ANOTHER TEMP CSV FILE
with open('data1.csv') as in_file:
    with open('data2.csv', 'w') as out_file:
        writer = csv.writer(out_file)
        for row in csv.reader(in_file):
            if row:
                writer.writerow(row)
#REMOVE EXTRA DATA
with open("data2.csv","r") as source:
    rdr= csv.reader( source )
    with open("dataOut.csv","w") as result:
        wtr= csv.writer( result )
        for r in rdr:
            wtr.writerow( (r[0], r[4], r[6], r[7], r[11], r[12], r[13], r[16]))


#IMPORT PLOT C DATA AS XML
r = requests.get(urlc, allow_redirects=True, verify=False)
open("data.xml", "wb").write(r.content)

#CONVERT XML TO A TEMP CSV
html = open("data.xml")
soup = BeautifulSoup(html, "lxml")
table = soup.find("table")
output_rows = []
for table_row in table.findAll('tr'):
    columns = table_row.findAll('td')
    output_row = []
    for column in columns:
        output_row.append(column.text)
    output_rows.append(output_row)   
with open('PlotCdata.csv', 'w') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerows(output_rows)

#ADD TO ANOTHER TEMP CSV BUT WITH APPENDED PLOT C COLUMN
with open('PlotCdata.csv','r') as csvinput:
    with open('data3.csv', 'w') as csvoutput:
        writer = csv.writer(csvoutput, lineterminator='\n')
        reader = csv.reader(csvinput)
        all = []
        row = next(reader)
        all.append(row)
        for row in reader:
            row.append('Plot C')
            all.append(row)
        writer.writerows(all)



#IMPORT PLOT D DATA AS XML
r = requests.get(urld, allow_redirects=True, verify=False)
open("data.xml", "wb").write(r.content)

#CONVERT XML TO TEMP CSV
html = open("data.xml")
soup = BeautifulSoup(html, "lxml")
table = soup.find("table")
output_rows = []
for table_row in table.findAll('tr'):
    columns = table_row.findAll('td')
    output_row = []
    for column in columns:
        output_row.append(column.text)
    output_rows.append(output_row)    
with open('PlotDdata.csv', 'w') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerows(output_rows)

#MERGE WITH TEMP CSV WITH PLOT C COLUMNS AND APPEND PLOT D
with open('PlotDdata.csv','r') as csvinput:
    with open('data3.csv', 'a') as csvoutput:
        writer = csv.writer(csvoutput, lineterminator='\n')
        reader = csv.reader(csvinput)
        all = []
        row = next(reader)
        all.append(row)
        for row in reader:
            row.append('Plot D')
            all.append(row)
        writer.writerows(all)

#REMOVE BLANK LINES INTO ANOTHER TEMP CSV FILE
with open('data3.csv') as in_file:
    with open('data4.csv', 'w') as out_file:
        writer = csv.writer(out_file)
        for row in csv.reader(in_file):
            if row:
                writer.writerow(row)

#REMOVE EXTRA DATA AND APPEND TO FINAL CSV
with open("data4.csv","r") as source:
    rdr= csv.reader( source )
    with open("dataOut.csv","a") as result:
        wtr= csv.writer( result )
        for r in rdr:
            wtr.writerow( (r[0], r[2], r[4], r[5], r[6], r[8], r[9], r[12]) )


#SORT BY DATE AND ADD HEADER
reader = csv.reader(open("dataOut.csv"))
sortedlist = sorted(reader, key = lambda row: datetime.strptime(row[6], '%b %d, %Y %I:%M:%S %p'), reverse=True)
writer = csv.writer(open('dataTemp.csv', 'w'), lineterminator='\n')
writer.writerow(['Document', 'Status', 'Paper Type', 'Paper Usage (sq. ft)', 'Ink Usage (mL)', 'UserID', 'Date', 'Printer Plot'])
writer.writerows(sortedlist)

#Remove Duplicates
import pandas as pd
toclean = pd.read_csv('dataTemp.csv')
deduped = toclean.drop_duplicates(["Date","Printer Plot"])
deduped.to_csv('data.csv')

#DELETE TEMP FILES
os.remove("PlotAdata.csv")
os.remove("PlotBdata.csv")
os.remove("PlotCdata.csv")
os.remove("PlotDdata.csv")
os.remove("data1.csv")
os.remove("data2.csv")
os.remove("data3.csv")
os.remove("data4.csv")
os.remove("dataOut.csv")
os.remove("dataTemp.csv")
os.remove('data.xml')
